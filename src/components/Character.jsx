import { slugify } from "@/utils/utils";
import Link from "next/link";

export function Character({ character }) {
  return (
    <Link href={`/character/${character?.id}`}>
      <div className="bg-gray-900 cursor-pointer hover:bg-gray-500 flex flex-col gap-5 p-5 justify-center items-center rounded-xl">
        <h2 className="text-2xl font-medium">{character.name}</h2>
        <img
          src={character.image}
          alt={character.name}
          className="w-42 h-42 rounded-full object-contain"
        />
        <>HOLA {character.id}</>
        <p className="text-xl font-medium">{`Origin: ${
          character.origin && character.origin.name
        }`}</p>
      </div>
    </Link>
  );
}
export default Character;
