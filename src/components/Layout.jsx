import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";

export const Layout = ({ children }) => {
  const router = useRouter();

  return (
    <div className={`h-screen flex flex-col justify-between`}>
      <div>
        <header className="bg-gray-800 p-5">
          <nav className="container mx-auto ">
            <Link href="/">
              <Image
                src="/assets/logo.svg"
                alt="Logo"
                width={300}
                height={300}
                className=" w-60 h-50 object-cover"
              />
            </Link>
          </nav>
        </header>
        <main className="container mx-auto p-5">{children}</main>
      </div>
      <footer className="bg-gray-800 text-white py-8 p-5 ">
        <div className="container mx-auto p-5 flex flex-col items-center">
          <h2 className="text-3xl font-bold mb-4">Stay Connected</h2>
          <p className="text-lg mb-6">
            Join us on social media for more adventures:
          </p>
          <p className="mt-6">© 2023 Rick & Morty Universe</p>
        </div>
      </footer>
    </div>
  );
};
