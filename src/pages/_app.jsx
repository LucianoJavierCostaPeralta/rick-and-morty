import { Layout } from "@/components/Layout";
import "@/styles/globals.css";
import Head from "next/head";

export default function App({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Rick & Morty</title>
        <link rel="shortcut icon" href="/assets/f.ico" type="image/x-icon" />
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />

        <meta
          name="description"
          content="Explore and visualize the characters from Rick and Morty in this fun interactive application."
        />
        <meta
          name="keywords"
          content="Rick and Morty, characters, application, explore, visualize"
        />
        <meta name="author" content="Luciano" />
        <meta property="og:image" content="/assets/f.ico" />
      </Head>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  );
}
