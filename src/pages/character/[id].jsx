import { useRouter } from "next/router";

const CharacterPage = ({ character }) => {
  const router = useRouter();
  return (
    <>
      <div className="grid grid-col-1 nd:grid-col-2 ">
        <div className="mx-auto bg-gray-900 p-5 rounded-lg my-10 shadow-md shadow-green-400">
          <div className="flex flex-col gap-5 items-center">
            <img
              src={character.image}
              className="w-full h-full object-cover rounded-full"
            />

            <div className="flex flex-col gap-4">
              <h1 className="text-3xl">{character.name}</h1>
              <p className="text-2xl">{`Status: ${character.status}`}</p>
              <p className="text-2xl">{`Species: ${character.species}`}</p>
              <p className="text-2xl">{`Gender: ${character.gender}`}</p>

              <p className="text-2xl">{`Location: ${character.location.name}`}</p>
            </div>
          </div>
          <button
            className="bg-green-500 hover:bg-green-600 text-white font-bold
                      py-4 px-6 rounded uppercase w-full mt-5"
            onClick={() => {
              router.push("/");
            }}
          >
            Go to Home
          </button>
        </div>
      </div>
    </>
  );
};

export default CharacterPage;

export async function getStaticPaths() {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API}/character`);
  const characters = await response.json();

  const paths = characters.results.map((character) => ({
    params: { id: character.id.toString() },
  }));

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_API}/character/${params.id}`
  );
  const character = await response.json();

  return {
    props: {
      character,
    },
  };
}
