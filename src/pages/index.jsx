import Character from "@/components/Character";
import { useState, useEffect } from "react";

function NavPage({ page, setPage }) {
  const isBackDisabled = page === 1;
  const isNextDisabled = page === 43;

  const handlePrevClick = () => {
    if (!isBackDisabled) {
      setPage(page - 1);
    }
  };

  const handleNextClick = () => {
    if (!isNextDisabled) {
      setPage(page + 1);
    }
  };

  return (
    <div className="flex flex-col justify-between items-center my-10 md:flex-row gap-10">
      <button
        className={`bg-green-500 hover-bg-green-600 text-white font-bold
                    py-4 px-6 rounded uppercase transition duration-300 ease-in-out w-full md-w-auto ${
                      isBackDisabled ? "opacity-50 cursor-not-allowed" : ""
                    }`}
        onClick={handlePrevClick}
        disabled={isBackDisabled}
      >
        Page {page}
      </button>

      <button
        className={`bg-green-500 hover-bg-green-600 text-white font-bold
                    py-4 px-6 rounded uppercase transition duration-300 ease-in-out w-full md-w-auto ${
                      isNextDisabled ? "opacity-50 cursor-not-allowed" : ""
                    }`}
        onClick={handleNextClick}
        disabled={isNextDisabled}
      >
        Page {page + 1}
      </button>
    </div>
  );
}

export function HomePage() {
  const [loading, setLoading] = useState(true);
  const [characters, setCharacters] = useState([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    async function fetchData() {
      const data = await fetch(
        `${process.env.NEXT_PUBLIC_API}/character?page=${page}`
      );
      const { results } = await data.json();
      setCharacters(results);
      setLoading(false);
      window.scrollTo(0, 0); // Scroll to the top of the page
    }
    fetchData();
  }, [page]);

  return (
    <>
      <h1 className="text-4xl md-text-5xl lg-text-6xl font-extrabold text-green-500 text-center mt-10">
        Rick & Morty Universe
      </h1>

      <NavPage page={page} setPage={setPage} />

      {loading ? (
        <div>Loading...</div>
      ) : (
        <div
          className={
            characters?.length > 0
              ? "grid grid-cols-1 md-grid-cols-2 lg-grid-cols-3 gap-5 my-5"
              : "grid-cols-1"
          }
        >
          {characters?.length > 0 ? (
            characters.map((character) => (
              <Character key={character.id} character={character} />
            ))
          ) : (
            <div className="grid grid-col-12 gap-4 my-5 items-center justify-center">
              <h2 className="text-3xl font-bold uppercase">No Characters</h2>
              <button
                className="bg-green-500 hover-bg-green-600 text-white font-bold
                      py-4 px-6 rounded uppercase w-full"
                onClick={() => setPage(1)}
              >
                Go Back to the First Page
              </button>
            </div>
          )}
        </div>
      )}

      <NavPage page={page} setPage={setPage} />
    </>
  );
}

export default HomePage;
