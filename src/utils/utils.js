export const slugify = (string) =>
  string.toLowerCase().trim().replaceAll(" ", "-");
